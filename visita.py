#!/usr/bin/python3
from persona import Persona
from almacen import Almacen

mialmacen = Almacen()
julian = Persona("Julian")
julieta = Persona("Julieta")

julian.visita(mialmacen)
julieta.visita(mialmacen)