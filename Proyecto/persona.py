class Persona(object):
    def __init__(self, nombre):
        self.nombre = nombre

    def visitar(self, servidor):
        print("Este es {0}.".format(self.nombre))
        self.buscar(servidor)
        self.terminar(servidor)
        print("Gracias!\n")

    def agregar(self, servidor):
        palabra             = input("Escribe la palabra para agregarla al diccionario: ").strip()
        if palabra:
            if servidor.palabraExistente(palabra) == False:
                # print(palabra)
                significado     = input("Escribe el significado: ").strip()
                servidor.agregarPalabra(self.nombre, palabra, significado)
            else:
                print("Palabra existente")
 
    def buscar(self, servidor):
        palabraABuscar = input("Escriba la palabra para buscarla en el diccionario: ").strip()
        if palabraABuscar:
            # servidor.palabraExistente(self.nombre, palabraABuscar)
            # try:
            if servidor.palabraExistente(palabraABuscar) == True:
                significado = servidor.buscarPalabra(self.nombre, palabraABuscar)

                print("-->\nEl significado de {0} : {1}".format(palabraABuscar,significado) )
            else:
                print("{0} --> !!! No existe en el diccionario".format(palabraABuscar)) 
                self.agregar(servidor)
                    # print("else{0} --> !!! No existe en el diccionario".format(palabraABuscar))   
            # except TypeError:
                
                # self.agregar(servidor)
            # else:
                # servidor.buscarPalabra(palabraABuscar)
               

    def terminar(self, servidor):
        servidor.cerrarArchivo()            
