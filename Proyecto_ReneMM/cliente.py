#!/usr/bin/python3
import sys
import Pyro4
from persona import Persona

# Author: Rene Martinez M
# version: 0.1.2
# Fecha: 11-Junio-2021 - 11:11 pm
# Sistemas Distribuidos

sys.excepthook = Pyro4.util.excepthook #Esta instrucción permite obtener más datos 
                                       #por parte de pyro en caso de que ocurra una excepción 
                                       #en el objeto remoto
miDiccionario = Pyro4.Proxy("PYRONAME:ejemplo.diccionario")
# julian = Persona("Julian")
julieta = Persona("Julieta")
# julian.visitar(miDiccionario)
julieta.visitar(miDiccionario)

