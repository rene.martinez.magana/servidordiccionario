#!/usr/bin/python3
import sys
import Pyro4
from persona import Persona
# from servidor import Diccionario
# uri = input("Ingresa el uri del servidor: ").strip()
sys.excepthook = Pyro4.util.excepthook #Esta instrucción permite obtener más datos 
                                       #por parte de pyro en caso de que ocurra una excepción 
                                       #en el objeto remoto

miDiccionario = Pyro4.Proxy("PYRONAME:ejemplo.diccionario")
julian = Persona("Julian")
# julieta = Persona("Julieta")
julian.visitar(miDiccionario)
# julieta.visitar(miDiccionario)
# mialmacen = Pyro4.Proxy("PYRONAME:ejemplo.almacen")
# julian = Persona("Julian")
# julieta = Persona("Julieta")
# julian.visita(mialmacen)
# julieta.visita(mialmacen)
