# Author: Rene Martinez M
# version: 0.1.2
# Fecha: 11-Junio-2021 - 11:11 pm
# Sistemas Distribuidos

class Persona(object):
    def __init__(self, nombre):
        self.nombre = nombre

    def visitar(self, servidor):
        print("Este es {0}.".format(self.nombre))
        self.buscar(servidor)
        self.terminar(servidor)
        print("Gracias!\n")

    def agregar(self, servidor):
        palabra             = input("Escribe la palabra para agregarla al diccionario: ").strip()
        if palabra:
            if servidor.palabraExistente(palabra) == False:
                significado     = input("Escribe el significado: ").strip()
                servidor.agregarPalabra(self.nombre, palabra, significado)
            else:
                print("Palabra existente")
 
    def buscar(self, servidor):
        palabraABuscar = input("Escriba la palabra para buscarla en el diccionario: ").strip()
        if palabraABuscar:
            if servidor.palabraExistente(palabraABuscar) == True:
                significado = servidor.buscarPalabra(self.nombre, palabraABuscar)

                print("-->\nEl significado de {0} : {1}".format(palabraABuscar,significado) )
            else:
                print("{0} --> !!! No existe en el diccionario".format(palabraABuscar)) 
                self.agregar(servidor)

    def terminar(self, servidor):
        servidor.cerrarArchivo()            
