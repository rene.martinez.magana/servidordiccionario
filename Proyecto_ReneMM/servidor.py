#!/usr/bin/python3
from sys import platform
import time
import operator
import Pyro4
import threading
@Pyro4.expose
@Pyro4.behavior(instance_mode="single")

# Author: Rene Martinez M
# version: 0.1.2
# Fecha: 11-Junio-2021 - 11:11 pm
# Sistemas Distribuidos

class Diccionario(object):
    """Clase encargada de alamcenar palabras y significados"""

    def __init__(self):
        """Constructor que inicializa el archivo donde estan las palabras y significados
            Aqui se inicializa el Diccionario
            Se llena el diccionario con los valores obtenidos del archivo DICCIONARIO.TXT
        """
        self.archivo = open('DICCIONARIO.txt', 'r+')
        self.inicio = self.archivo.seek(0)
        self.diccionario = {}
        self.llenarDiccionario()

    def listar(self):
        """Este metodo se encarga de listar todos los valores del diccionario generado.
            La impresion es clave | valor
        """
        for clave, valor in self.diccionario.items():
            print( clave,"|", valor )

    def agregarPalabra(self,nombre,palabra, significado):
        """Metodo encargado de agregar una nueva palabra 
            Este metodo, agrega la palabra al diccionario, mientras esta en ejecucion
            LLama al metodo que ordena el diccionario
        """
        if not(palabra in self.diccionario) :
            self.diccionario[palabra] = significado+"\n"     
        else:
            print("{0} Ya existe en el diccionario".format(palabra))
        print("{0} guardando palabra {1}.".format(nombre,palabra))
        #cada vez que se agregue una palabra el diccionario se reordena automaticamente
        self.ordenarDiccionario()

    def buscarPalabra(self,nombre, palabra):
        """Metodo encargado de buscar una palabra
            La busqueda se realiza por clave, mediante le metodo get() que lanza una excepccion si 
            la coincidencia es False
        """
        busqueda = self.diccionario.get(palabra, str(palabra)+" No existe en el diccionario")
        significado = self.diccionario[palabra]
        print("{0} Se encontro la palabra {1}: {2}".format(nombre,palabra, significado)) 
        return significado

    def eliminarPalabra(self,nombre, palabra):
        """ Metodo encargado de eliminar una palabra.
            Para eliminar la palabra, hace uso del metodo buscarPalabra()
            Si la busqueda es exitosa, elimina el valor mediante la clave
        """
        if self.buscarPalabra(palabra) != None:
            del self.diccionario[palabra]
            print("{0} eliminando palabra {1}.".format(nombre, palabra))
        else:
            print("La palabra " + str(palabra) + " no esta en el diccionario")

    def llenarDiccionario(self):
        """Metodo encargado de llenar el diccionario con los valores del archivo DICCIONARIO.TXT"""
        with self.archivo:
            for linea in self.archivo:
                auxPalabras = linea.split('|')
                self.diccionario[auxPalabras[0]] = auxPalabras[1]
        print("Diccionario llenado con exito")


    def llenarArchivo(self):
        """Metodo encargado de llenar el archivo, cada vez que se requiera guardar cambios y se finalice la ejecucion"""
        with open('DICCIONARIO.TXT','w') as self.archivo:
            for clave, valor in self.diccionario.items():
                linea = str(clave) + "|" +str(valor) 
                self.archivo.write(linea)
                self.archivo.truncate()
        
    def cerrarArchivo(self):
        """Metodo encargado de cerrar el archivo DICCIONARIO.TXT """
        self.llenarArchivo()
        self.archivo.close()
        
    def ordenarDiccionario(self):
        """Metodo encargado de ordenar el diccionario en tiempo de ejecucion"""
        #generamos una lista ordenada por clave del diccionario
        listaDiccionario = sorted(self.diccionario.items(), key=operator.itemgetter(0))
        #reordenamos los valores en el mismo diccionario
        self.diccionario = {k:v for k, v in listaDiccionario}

    def palabraExistente(self, palabra):
        """Metodo encargado de buscar solo la coincidencia de la palabra"""
        if  palabra in self.diccionario:
            return True
        else:
            return False



def main():
    """Metodo principal de ejecucion
        Aqui se inicializa el servidor y se coloca una barrera para sincronizar los hilos     """
    Pyro4.Daemon.serveSimple(
    {
        Diccionario: "ejemplo.diccionario"
    },
    host = "127.0.0.1",
    ns = True)
    barrier.wait()

if __name__=="__main__":
    """Aqui creamos los hilos para cubrir los posibles llamdos de los clientes hacia el servidor"""
    while True:
        count = 100
        barrier = threading.Barrier(count)
        threads = [
            threading.Thread(
                target=main, name=f'T{name}', args=(local, barrier)
            )for name in range(count)
        ]
        for t in threads:
            t.start()