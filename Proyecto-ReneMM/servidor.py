#!/usr/bin/python3
from sys import platform
import time
import operator
import Pyro4
@Pyro4.expose
@Pyro4.behavior(instance_mode="single")

class Diccionario(object):
    def __init__(self):
        self.archivo = open('DICCIONARIO.txt', 'r+')
        self.inicio = self.archivo.seek(0)
        self.diccionario = {}
        self.llenarDiccionario()

    def listar(self):
        for clave, valor in self.diccionario.items():
            print( clave,"|", valor )

    def agregarPalabra(self,nombre,palabra, significado):
        if not(palabra in self.diccionario) :
            self.diccionario[palabra] = significado+"\n"     
        else:
            print("{0} Ya existe en el diccionario".format(palabra))
        print("{0} guardando palabra {1}.".format(nombre,palabra))
        #cada vez que se agregue una palabra el diccionario se reordena automaticamente
        self.ordenarDiccionario()

    def buscarPalabra(self,nombre, palabra):
        busqueda = self.diccionario.get(palabra, str(palabra)+" No existe en el diccionario")
        significado = self.diccionario[palabra]
        print("{0} Se encontro la palabra {1}: {2}".format(nombre,palabra, significado)) 
        return significado

    def eliminarPalabra(self,nombre, palabra):
        if self.buscarPalabra(palabra) != None:
            del self.diccionario[palabra]
            print("{0} eliminando palabra {1}.".format(nombre, palabra))
        else:
            print("La palabra " + str(palabra) + " no esta en el diccionario")

    def llenarDiccionario(self):
        with self.archivo:
            for linea in self.archivo:
                auxPalabras = linea.split('|')
                self.diccionario[auxPalabras[0]] = auxPalabras[1]
        print("Diccionario llenado con exito")
        
                # print(auxPalabras[1])
            # print(palabras)    
            
            # self.diccionario[]
                # for palabra in palabras:
                #     print(palabra.strip())

    def llenarArchivo(self):
        with open('DICCIONARIO.TXT','w') as self.archivo:
            for clave, valor in self.diccionario.items():
                linea = str(clave) + "|" +str(valor) 
                self.archivo.write(linea)
                self.archivo.truncate()
                # print(clave, "|", valor)
        
    def cerrarArchivo(self):
        self.llenarArchivo()
        self.archivo.close()
        
    def ordenarDiccionario(self):
        #generamos una lista ordenada por clave del diccionario
        listaDiccionario = sorted(self.diccionario.items(), key=operator.itemgetter(0))
        #reordenamos los valores en el mismo diccionario
        self.diccionario = {k:v for k, v in listaDiccionario}

    def palabraExistente(self, palabra):
        if  palabra in self.diccionario:
            return True
        else:
            return False



def main():
    Pyro4.Daemon.serveSimple(
    {
        Diccionario: "ejemplo.diccionario"
    },
    host = "127.0.0.1",
    ns = True)

if __name__=="__main__":
    main()
# diccionario = Diccionario()
# diccionario.buscarPalabra("Jose","Voto")
# print( diccionario.palabraExistente("Voto") )
# # print(diccionario.listar())

# time.sleep(3)
# diccionario.buscarPalabra("Jose","Aaczino")
#LLenamos el diccionario con las palabras del archivo .txt
# diccionario.llenarDiccionario()

# diccionario.listar()

# print("agregando palabra")
# time.sleep(2)
# diccionario.agregarPalabra("Jose", "Aaczino", "Rapero")
# time.sleep(2)
# diccionario.listar()
# time.sleep(2)
# time.sleep(3)
# diccionario.listar()

# print("buscando palabra Azzczino")
# print(diccionario.buscarPalabra("Aaaczino"))
# time.sleep(5)
# diccionario.listar()
# print("buscando la palabra ZZZ")
# print(diccionario.buscarPalabra("ZZZ"))
# diccionario.eliminarPalabra("Jose","Aaaczino")
# time.sleep(5)
# diccionario.listar()

# diccionario.listar()
# print(diccionario.buscarPalabra("Abandonado"))
# diccionario.guardarPalabra("Jose","Atenea", "DJ-frestyler\n")
# time.sleep(5)
# diccionario.listar()
# print( diccionario.buscarPalabra("Aczino") )
# diccionario.eliminarPalabra("JHose","Aczino")

# diccionario.llenarArchivo()

#Cerramos el archivo .txt
# diccionario.cerrarArchivo()

# def main():
#     Pyro4.Daemon.serveSimple(
#             {
#                 Diccionario: "ejemplo.diccionario"
#             },
#             host = "127.0.1.72",
#             # host = servidor.uacm.edu.mx
#             ns = True) #ns = NameServer

# if __name__=="__main__":
#     main()
