#!/usr/bin/python3
import Pyro4
@Pyro4.expose
@Pyro4.behavior(instance_mode="single") #instance_mode=percall #instance_mode=session

class Almacen(object):
    def __init__(self):
        self.inventario = ["silla", "scooter", "lampara", "laptop", "mesa"]

    def lista(self):
        return self.inventario

    def devuelve (self,nombre, articulo):
        self.inventario.remove(articulo)
        print("{0} recuperando {1}.".format(nombre, articulo))

    def guarda(self,nombre,articulo):
        self.inventario.append(articulo)
        print("{0} guardando {1}.".format(nombre, articulo))

def main():
    Pyro4.Daemon.serveSimple(
            {
                Almacen: "ejemplo.almacen"
            },
            ns = False)

if __name__=="__main__":
    main()
