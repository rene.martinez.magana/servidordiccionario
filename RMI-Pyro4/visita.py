#!/usr/bin/python3
import Pyro4
from persona import Persona

uri = input("Ingresa el uri del almacen: ").strip()

mialmacen = Pyro4.Proxy(uri)
julian = Persona("Julian")
julieta = Persona("Julieta")
julian.visita(mialmacen)
julieta.visita(mialmacen)
