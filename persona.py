class Persona(object):
    def __init__(self, nombre):
        self.nombre = nombre
    
    def visita(self, almacen):
        print("Este es {0}.".format(self.nombre))
        self.deposita(almacen)
        self.recupera(almacen)
        print("Gracias")

    def deposita(self, almacen):
        print("El almacen contiene: ", almacen.lista())
        articulo = input("Escriba que desea depositar (o vacio): ").strip()
        if articulo:
            almacen.guarda(self.nombre, articulo)

    def recupera(self, almacen):
        print("El almacen contiene: ", almacen.lista())
        articulo = input("Escriba que desea recuperar (o vacio): ").strip()
        if articulo:
            try:
                almacen.devuelve(self.nombre, articulo)
            except:
                print("No fue posible recuperar: '{0}'. Comuniquese al 01800-554544".format(articulo))
