class Almacen(object):
    def __init__(self):
        self.inventario = ["silla", "scooter", "lampara", "laptop", "mesa"]

    def lista(self):
        return self.inventario

    def devuelve(self, nombre, articulo):
        self.inventario.remove(articulo)
        print("{0} recuperando {1}.".format(nombre,articulo))
        
    def guarda(self,nombre,articulo):
        self.inventario.append(articulo)
        print("{0} guardando {1}.".format(nombre, articulo))    
