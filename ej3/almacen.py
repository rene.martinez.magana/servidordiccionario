#!/usr/bin/python3
import Pyro4
@Pyro4.expose
@Pyro4.behavior(instance_mode="single")

class Almacen(object):
    def __init__(self):
        self.inventario = ["silla", "scooter", "lampara", "laptop", "mesa"]

    def lista(self):
        return self.inventario

    def devuelve (self,nombre, articulo):
        self.inventario.remove(articulo)
        print("{0} recuperando {1}.".format(nombre, articulo))

    def guarda(self,nombre,articulo):
        self.inventario.append(articulo)
        print("{0} guardando {1}.".format(nombre, articulo))

def main():
    Pyro4.Daemon.serveSimple(
            {
                Almacen: "ejemplo.almacen"
            },
            host = "127.0.1.72",
            # host = servidor.uacm.edu.mx
            ns = True) #ns = NameServer

if __name__=="__main__":
    main()
