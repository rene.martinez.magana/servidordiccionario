#!/usr/bin/python3
import sys
import Pyro4
from persona import Persona

sys.excepthook = Pyro4.util.excepthook #Esta instrucción permite obtener más datos 
                                       #por parte de pyro en caso de que ocurra una excepción 
                                       #en el objeto remoto

mialmacen = Pyro4.Proxy("PYRONAME:ejemplo.almacen")
julian = Persona("Julian")
julieta = Persona("Julieta")
julian.visita(mialmacen)
julieta.visita(mialmacen)
